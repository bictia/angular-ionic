import { Component } from '@angular/core';
import {UserService} from '../service/user.service'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  users: any[];

  constructor(public userService: UserService) {}

  ngOnInit(){

    this.userService.getUsers().subscribe((data)=>{
      this.users = data['results'];
      console.log(this.users);
            
    });

  }

}
