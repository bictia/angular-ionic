import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage, AngularFireStorageReference } from 'angularfire2/storage';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  image: string;
  constructor(private geolocation: Geolocation,
    private fireStore: AngularFirestore,
    private storage: Storage,
    private firestorage: AngularFireStorage,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    private camera: Camera) { }

  ref: AngularFireStorageReference;

  uploadFile(event) {
    console.log('1');
    
    const file = event.target.files[0];
    const filePath = new Date().toISOString();
    const ref = this.firestorage.ref(filePath);
    console.log('2');
    ref.put(file).then((data)=>{
      console.log(data);      
    }).catch((error)=>{
      console.error(error);      
    });
    //let name = this.ref(new Date().toDateString);

  }

  downloadFile(){
    const ref = this.firestorage.ref('2020-03-11T20:49:19.891Z');
    ref.getDownloadURL().subscribe((data)=>{
      console.log(data);
    })
    
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((data) => {
      //let base64Image = 'data:image/jpeg;base64,' + data;
      //this.presentImage(base64Image);
      this.image = 'data:image/jpeg;base64,' + data;

    }).catch((error) => {
      console.log(error);

    })
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Borrar elemento',
      message: 'Realmente quieres borrar este elemento',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.storage.set('cancel', true);
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Borrar',
          handler: () => {
            this.storage.set('delete', true);
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }


  async presentImage(image) {
    const alert = await this.alertController.create({
      header: 'Ubicación',
      message: image,
      buttons: [{
        text: 'OK',
        handler: () => {
          //console.log(resp.coords.latitude);
          //console.log(resp.coords.longitude);

        }
      }]
    });
    await alert.present();
  }

  async presentLocationAlert(resp) {
    const alert = await this.alertController.create({
      header: 'Ubicación',
      message: 'Su ubicación es: \nLatitud: ' +
        resp.coords.latitude + '\nLongitud: ' +
        resp.coords.longitude,
      buttons: [{
        text: 'OK',
        handler: () => {
          //console.log(resp.coords.latitude);
          //console.log(resp.coords.longitude);

        }
      }]
    });
    await alert.present();
  }

  async presentActionSheet() {

    let myClass = this.fireStore.collection('class', ref => {
      let resultado = ref.where('room', '==', '101');
      return resultado;
    }).valueChanges().subscribe((data) => {
      console.log(data);
    })



    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      buttons: [{
        text: 'Location',
        role: 'destructive',
        icon: 'location',
        handler: () => {
          this.geolocation.getCurrentPosition().then((resp) => {
            // resp.coords.latitude
            // resp.coords.longitude
            //console.log(resp.coords.latitude);
            //console.log(resp.coords.longitude);
            this.presentLocationAlert(resp);
            let data = {}

            data['date'] = new Date();
            data['latitude'] = resp.coords.latitude;
            data['longitude'] = resp.coords.longitude;
            this.fireStore.collection('locations').add(data)
              .then((data) => {
                console.log(data);
              }).catch((error) => {
                console.log(error);
              })
          }).catch((error) => {
            console.log('Error getting location', error);
          });
        }
      }, {
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
          this.presentAlertConfirm();
        }
      }, {
        text: 'Share',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Play (open modal)',
        icon: 'arrow-dropright-circle',
        handler: () => {
          console.log('Play clicked');
        }
      }, {
        text: 'Favorite',
        icon: 'heart',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  // ref: AngularFireStorageReference;
  // task: AngularFireUploadTask;


  // upload(event) {
  //   const id = Math.random().toString(36).substring(2);
  //   this.ref = this.firestorage.ref(id);
  //   this.ref.put(event.target.files[0]).then((data)=>{
  //     return data;
  //   }).catch((error)=>{
  //     console.error(error);      
  //   })
  // }

}
