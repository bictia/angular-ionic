import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';



@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(private storage: Storage,
    private firebaseAuthentication: FirebaseAuthentication,
    public alertController: AlertController) { }

  value: any;
  email: string;
  password: string;
  phone: string;
  id_code: string;
  code: string;

  sendCode() {
    console.log(this.phone);
    this.firebaseAuthentication.verifyPhoneNumber(this.phone, 120000)
      .then((res: any) => {
        this.showMessage(res);
        this.storage.set('auth_sms', res);
      })
      .catch((error: any) => this.showMessage(error));
  }

  validateCode() {
    console.log(this.id_code);
    console.log(this.code);

    this.storage.get('auth_sms').then((data) => {
      this.firebaseAuthentication.signInWithVerificationId(data, Number(this.code))
      .then((res: any) => {
        this.showMessage(res)
      })
      .catch((error: any) => this.showMessage(error));

    }).catch((error) => {
      console.log(error)
    })


    
  }

  loginEmail() {
    console.log(this.email);
    console.log(this.password);


    this.firebaseAuthentication.signInWithEmailAndPassword(this.email, this.password)
      .then((res: any) => this.showMessage(res))
      .catch((error: any) => this.showMessage(error));

  }

  async showMessage(message) {
    const alert = await this.alertController.create({
      header: 'Mensaje de consola',
      message: message,
      buttons: [{
        text: 'OK',
        handler: () => {
          //console.log(resp.coords.latitude);
          //console.log(resp.coords.longitude);

        }
      }]
    });
    await alert.present();
  }

  saveValue(value: any) {
    this.storage.set('test', value);
  }

  ngOnInit() {

    this.storage.get('delete').then((data) => {
      console.log(data);

    }).catch((error) => {
      console.log(error)
    })

    this.storage.get('test').then((data) => {
      this.value = data;
      console.log(data);

    }).catch((error) => {
      console.log(error)
    })
  }

}
